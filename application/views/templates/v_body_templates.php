    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        
        <!-- ========================== MANAGEMENT NAVBAR ======================== -->
        <ul class="sidebar-menu">
          <?php 
              
              $sessi = $this->session->userdata('id');
              $manager  = $this->session->userdata('level') == 'manager';
              $karyawan = $this->session->userdata('level') == 'karyawan';
              $client   = $this->session->userdata('level') == 'client';

              if($sessi == $manager){
         ?>
          <li class="nav-sidebar">
            <a class="" href="<?= base_url(); ?>index.php/manager/">
                <i class="icon_house_alt"></i>
                <span>Dashboard</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="<?= base_url(); ?>index.php/manager/data_karyawan/" class="">
                <i class="fa fa-book"></i>
                <span>Data Karyawan (v)</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="<?= base_url(); ?>index.php/" class="">
                <i class="fa fa-book"></i>
                <span>Rekap Kehadiran</span>
            </a>
          </li>
          <li>
            <a class="" href="<?= base_url(); ?>index.php/manager/task_karyawan/">
                <i class="icon_document_alt"></i>
                <span>Task Karyawan (v)</span>
            </a>
          </li>
          <li>
            <a class="" href="">
                <i class="fa fa-dollar "></i>
                <span>Gaji Karyawan</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="<?= base_url() ?>index.php/manager/permintaan_ijin/ " class="">
                <i class="fa fa-car"></i>
                <span>Permintaan Izin (v)</span>
            </a>
          </li>
          <?php }elseif($sessi == $karyawan){ ?>
          <!-- ========================== KARYAWAN NAVBAR ======================== -->

          <li class="sub-menu">
            <a href="<?= base_url() ?>index.php/karyawan/" class="">
                <i class="fa fa-archive"></i>
                <span>Absensi (v)</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="<?= base_url() ?>index.php/karyawan/lembur/" class="">
                <i class="fa fa-bullhorn"></i>
                <span>Lembur (v)</span>
            </a>
          </li>
          <li>
            <a class="" href="<?= base_url() ?>index.php/karyawan/to_do_list/">
                <i class="icon_document_alt"></i>
                <span>To Do List (v)</span>
            </a>
          </li>
          <li>
            <a class="" href="<?= base_url() ?>index.php/karyawan/tabung_hari/">
                <i class="fa fa-bookmark"></i>
                <span>Tabung Hari (v)</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="" class="">
                <i class="fa fa-reply"></i>
                <span>Ganti Hari</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="<?= base_url() ?>index.php/karyawan/ajukan_ijin" class="">
                <i class="fa fa-car"></i>
                <span>Ajukan Izin (v)</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="<?= base_url(); ?>index.php/karyawan/rekap_hadir" class="">
                <i class="fa fa-book"></i>
                <span>Rekap Kehadiran (v)</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="" class="">
                <i class="fa fa-dollar"></i>
                <span>Slip Gaji</span>
            </a>
          </li>

          <!-- ========================== CLIENT NAVBAR ======================== -->
          <?php }else{ ?>
          <li class="sub-menu">
            <a href="<?= base_url() ?>index.php/client/" class="">
                <i class="icon_document_alt"></i>
                <span>Task Karyawan </span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="<?= base_url() ?>index.php/client/kehadiran/" class="">
                <i class="fa fa-book"></i>
                <span>Kehadiran </span>
            </a>
          </li>
          <?php } ?>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->

    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-laptop"></i> PT ---</h3>
          </div>
        </div>

        <div class="content">
          
          <?= $data ?>

        </div>              
  </section>