<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
  <meta name="author" content="GeeksLabs">
  <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
  <link rel="shortcut icon" href="<?= base_url() ?>view_template/img/favicon.png">

  <title>Halaman Management</title>

  <!-- Bootstrap CSS -->
  <link href="<?= base_url() ?>view_template/css/bootstrap.min.css" rel="stylesheet">
  <!-- bootstrap theme -->
  <link href="<?= base_url() ?>view_template/css/bootstrap-theme.css" rel="stylesheet">
  <!--external css-->
  <!-- font icon -->
  <link href="<?= base_url() ?>view_template/css/elegant-icons-style.css" rel="stylesheet" />
  <link href="<?= base_url() ?>view_template/css/font-awesome.min.css" rel="stylesheet" />
  <!-- full calendar css-->
  <link href="<?= base_url() ?>view_template/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
  <link href="<?= base_url() ?>view_template/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
  <!-- easy pie chart-->
  <link href="<?= base_url() ?>view_template/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen" />
  <!-- owl carousel -->
  <link rel="stylesheet" href="<?= base_url() ?>view_template/css/owl.carousel.css" type="text/css">
  <link href="<?= base_url() ?>view_template/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
  <!-- Custom styles -->
  <link rel="stylesheet" href="<?= base_url() ?>view_template/css/fullcalendar.css">
  <link href="<?= base_url() ?>view_template/css/widgets.css" rel="stylesheet">
  <link href="<?= base_url() ?>view_template/css/style.css" rel="stylesheet">
  <link href="<?= base_url() ?>view_template/css/style-responsive.css" rel="stylesheet" />
  <link href="<?= base_url() ?>view_template/css/xcharts.min.css" rel=" stylesheet">
  <link href="<?= base_url() ?>view_template/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
  <!-- =======================================================
    Theme Name: NiceAdmin
    Theme URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
  ======================================================= -->
</head>

<body>
  <!-- container section start -->
  <section id="container" class="">


    <header class="header dark-bg">
      <div class="toggle-nav">
        <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
      </div>

      <!--logo start-->
      <a href="index.html" class="logo">UI <span class="lite">Admin</span></a>
      <!--logo end-->

      <div class="top-nav notification-row">
        <!-- notificatoin dropdown start-->
        <ul class="nav pull-right top-menu">
          <!-- user login dropdown start-->
          
              <li>
                <a href="<?php echo base_url()."index.php/login/logout/" ?>"><i class="icon_key_alt"></i> Log Out</a>
              </li>
            
          <!-- user login dropdown end -->
        </ul>
        <!-- notificatoin dropdown end-->
      </div>
    </header>
    <!--header end-->