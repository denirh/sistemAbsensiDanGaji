<section>
	<div class="row">
		<div class="col-lg-6">
		  <header class="panel-heading">
		    History Absensi
		  </header>
		  <table class="table table-striped table-advance table-hover table-bordered" style="margin-top: 13px;">
		      <tbody>
		        <tr>
		          <th><i class="icon_profile"></i> ID</th>
		          <th><i class="fa fa-archive"></i> Tanggal</th>
		          <th><i class="icon_mail_alt"></i> Keterangan</th>
		        </tr>
		        <?php
		            foreach ($hadir_karyawan as $row) {
		        ?>
		        <tr>
		          <td class="text-center"><?= $row->presensi_karyawan_id ?></td>
		          <td><?= $row->presensi_date ?></td>
		          <td><?= $row->presensi_type ?></td>
		        </tr>
		        <?php } ?>
		      </tbody>
		    </table>
		</div>
	</div>
</section>