<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
	class Client extends MX_Controller
{

function __construct() {
	parent::__construct();
	$this->load->model('mdl_aman');
	$this->load->model('mdl_client');
}

function kehadiran(){
	$show_data = $this->mdl_client->tabel_presensi(); // menampilkan data tabel
	$data['hadir_karyawan'] = $show_data;
	$data['data'] = $this->load->view('v_rekap', $data, TRUE);
	$this->load->view('templates/v_header_templates');
	$this->load->view('templates/v_body_templates', $data);
	$this->load->view('templates/v_footer_templates');
}

function index(){
	$this->mdl_aman->halaman_aman();
	$show_data = $this->mdl_client->tabel_task(); // menampilkan data tabel
	$data['task_karyawan'] = $show_data;
	$data['data'] = $this->load->view('v_lihat_task', $data, TRUE);
	$this->load->view('templates/v_header_templates');
	$this->load->view('templates/v_body_templates', $data);
	$this->load->view('templates/v_footer_templates');
}



}
