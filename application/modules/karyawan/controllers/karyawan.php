<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
	class Karyawan extends MX_Controller
{

function __construct() {
	parent::__construct();
	$this->load->model('mdl_karyawan');
	$this->load->model('mdl_aman');
	$this->load->helper('url');
}

function update_tabung_hari(){
	
	$data = array(
		'presensi_desc_tabung_hari' => $this->input->post('desc_tabung')
	);
 
	$where = array(
		'presensi_id' => $this->input->post('id_pres')
	);
 
	$this->mdl_karyawan->update_nabung($where,$data,'presensi');
	redirect('karyawan/tabung_hari/');
}

function edit_tabung_hari($id){
	$where = array('presensi_id' => $id);
	$data['nabung'] = $this->mdl_karyawan->edit_nabung($where,'presensi')->result();
	$data['data'] = $this->load->view('v_tabung_hari_edit',$data, TRUE);
	$this->load->view('templates/v_header_templates');
	$this->load->view('templates/v_body_templates', $data);
	$this->load->view('templates/v_footer_templates');
}

function create_tabung_hari(){
	$this->load->helper('form');

	$data = array(
		'presensi_karyawan_id'      => $this->session->userdata('id'),
		'presensi_tabung_hari'      => $this->input->post('tanggal_tabung'),
		'presensi_desc_tabung_hari' => $this->input->post('desc_tabung'),
		'presensi_type'             => $this->input->post('tabungan')
	);

	$this->mdl_karyawan->input_tabung_hari($data,'presensi');

	redirect('karyawan/tabung_hari/');
}

function tabung_hari(){
	$usersession = $this->session->userdata('id');
	$show_data = $this->mdl_karyawan->show_tabung_hari(" WHERE presensi_karyawan_id='$usersession' AND presensi_type='nabung' "); 

	$data['tabung_history'] = $show_data;
	$data['data'] = $this->load->view('v_tabung_hari', $data, TRUE);
	$this->load->view('templates/v_header_templates');
	$this->load->view('templates/v_body_templates', $data);
	$this->load->view('templates/v_footer_templates');
}

function create_lembur(){
	$this->load->helper('form');

	$data = array(
		'presensi_karyawan_id' => $this->session->userdata('id'),
		'presensi_date'        => $this->input->post('tanggal_lembur'),
		'presensi_lama_lembur' => $this->input->post('lama_lembur'),
		'presensi_type'        => $this->input->post('lemburan'),
		'presensi_desc_lembur' => $this->input->post('task_lembur')
	);

	$this->mdl_karyawan->input_lembur($data,'presensi');

	redirect('karyawan/lembur/');
}

function lembur(){	
	$usersession = $this->session->userdata('id');
	$show_data = $this->mdl_karyawan->history_lembur( " WHERE presensi_karyawan_id='$usersession' AND presensi_type='lembur' "); // menampilkan data tabel
	$data['lembur_history'] = $show_data;
	$data['data'] = $this->load->view('v_lembur', $data, TRUE);
	$this->load->view('templates/v_header_templates');
	$this->load->view('templates/v_body_templates', $data);
	$this->load->view('templates/v_footer_templates');
}

function hapus_todolist($id){
	$where = array('task_id' => $id);
	$this->mdl_karyawan->hapus_todolist($where,'task');
	redirect('karyawan/to_do_list/');
}

function update_todolist(){
	
	$data = array(
		'task_desc'    => $this->input->post('todolist'),
		'task_tanggal' => $this->input->post('tanggal_task'),
		'task_percent' => $this->input->post('status')
	);
 
	$where = array(
		'task_id' => $this->input->post('id_task')
	);
 
	$this->mdl_karyawan->update_todolist($where,$data,'task');
	redirect('karyawan/to_do_list/');
}

function edit_todolist($id){
	$data['array_option'] = array('bug','proses','selesai');
	$where = array('task_id' => $id);
	$data['todolist'] = $this->mdl_karyawan->edit_todolist($where,'task')->result();
	$data['data'] = $this->load->view('v_todolist_edit',$data, TRUE);
	$this->load->view('templates/v_header_templates');
	$this->load->view('templates/v_body_templates', $data);
	$this->load->view('templates/v_footer_templates');
}

function create_todolist(){
	$this->load->helper('form');

	$data = array(
		'task_presensi_id' => $this->session->userdata('id'),
		'task_tanggal'     => $this->input->post('tanggal_task'),
		'task_desc'        => $this->input->post('todolist'),
		'task_percent'     => $this->input->post('status')
	);

	$this->mdl_karyawan->input_todolist($data,'task');

	redirect('karyawan/to_do_list/');
}

function to_do_list(){	
	$usersession = $this->session->userdata('id');
	$show_data = $this->mdl_karyawan->tabel_task( " WHERE task_presensi_id='$usersession' "); // menampilkan data tabel
	$data['task'] = $show_data;
	$data['data'] = $this->load->view('v_todolist', $data, TRUE);
	$this->load->view('templates/v_header_templates');
	$this->load->view('templates/v_body_templates', $data);
	$this->load->view('templates/v_footer_templates');
}

function rekap_hadir(){	
	// $show_data = $this->mdl_karyawan->tabel_presensi(); // menampilkan data tabel
	// $data['absen_history'] = $show_data;
	$data['data'] = $this->load->view('v_rekap_hadir', '', TRUE);
	$this->load->view('templates/v_header_templates');
	$this->load->view('templates/v_body_templates', $data);
	$this->load->view('templates/v_footer_templates');
}

function create_req_izin(){
	$this->load->helper('form');

	$data = array(
		'presensi_karyawan_id' => $this->session->userdata('id'),
		'presensi_acc_ijin'    => $this->input->post('tanggal_ijin'),
		'presensi_desc_ijin'   => $this->input->post('alasan'),
		'presensi_ijin_status' => $this->input->post('status')
	);

	$this->mdl_karyawan->input_req_ijin($data,'presensi');

	redirect('karyawan/ajukan_ijin/');
}

function ajukan_ijin(){	
	$usersession = $this->session->userdata('id');
	$show_data = $this->mdl_karyawan->lihat_ijin( " WHERE presensi_karyawan_id='$usersession' AND presensi_ijin_status "); // menampilkan data tabel
	$data['ijin_history'] = $show_data;
	$data['data'] = $this->load->view('v_req_ijin', $data, TRUE);
	$this->load->view('templates/v_header_templates');
	$this->load->view('templates/v_body_templates', $data);
	$this->load->view('templates/v_footer_templates');
}

function create_data(){
	$this->load->helper('form');

	$data = array(
		'presensi_karyawan_id' => $this->session->userdata('id'),
		'presensi_date'        => $this->input->post('tanggal_hadir'),
		'presensi_type'        => $this->input->post('type')
	);

	$this->mdl_karyawan->input_data($data,'presensi');

	redirect('karyawan/');
}

function index(){
	$this->mdl_aman->halaman_aman();
	$usersession = $this->session->userdata('id');
	$show_data = $this->mdl_karyawan->tabel_presensi(" WHERE presensi_karyawan_id='$usersession' order by presensi_karyawan_id desc"); // menampilkan data tabel

	$data['absen_history'] = $show_data;
	$data['data'] = $this->load->view('v_absen', $data, TRUE);
	$this->load->view('templates/v_header_templates');
	$this->load->view('templates/v_body_templates', $data);
	$this->load->view('templates/v_footer_templates');
}


}
