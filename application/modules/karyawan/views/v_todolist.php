<section>
  <div class="row">
	  <div class="col-lg-12" style="margin-top: 20px;">
      <section class="panel">
        <header class="panel-heading">
          Informasi Task
        </header>

        <table class="table table-striped table-advance table-hover">
          <tbody>
            <tr>
              <th><i class="icon_profile"></i> ID</th>
              <th>To Do List</th>
              <th>Tanggal</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
            <?php
            	foreach ($task as $row) {
                $status = $row->task_percent;
                $format = date('d F Y', strtotime($row->task_tanggal ));

                  if($status == 'proses'){
                    $status_label = 'info';
                    $status_desc  = 'Proses';
                  }elseif($status == 'selesai'){
                    $status_label = 'success';
                    $status_desc  = 'Selesai';
                  }elseif($status == 'bug'){
                    $status_label = 'danger';
                    $status_desc  = 'Ada Bug';
                  }else{
                    $status_label = '';
                    $status_desc  = '';
                  }
            ?>
            <tr>
              <td><?= $row->task_presensi_id ?></td>
              <td><?= $row->task_desc ?></td>
              <td><?= $format ?></td>
              <td>
                  <span class="label label-<?php echo $status_label ?>" style="font-size: 12px; border-radius: 0;"><?php echo $status_desc ?></span>
              </td>
              <td>
                <div class="btn-group">
                  <a class="btn btn-success" href="<?= base_url() ?>index.php/karyawan/edit_todolist/<?= $row->task_id ?>"><i class="fa fa-edit"></i></a>
                  <a class="btn btn-danger" href="<?= base_url() ?>index.php/karyawan/hapus_todolist/<?= $row->task_id ?>"><i class="icon_close_alt2"></i></a>
                </div>
              </td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </section>
    </div>

    <div class="col-lg-12">
      <!-- Form Todolist -->
        <header class="panel-heading">
          Buat To Do List
        </header>
        <div class="panel-body">
          <div class="form">
            <form class="form-validate form-horizontal" id="feedback_form" method="POST" action="<?= base_url() ?>index.php/karyawan/create_todolist/">
              <div class="form-group ">
                <label for="task" class="control-label col-lg-2">Tanggal <span class="required">*</label>
                <div class="col-lg-10">
                  <input class="form-control" type="date" id="task" name="tanggal_task" required>
                </div>
              </div>
              <div class="form-group ">
                <label for="task" class="control-label col-lg-2">To Do List <span class="required">*</label>
                <div class="col-lg-10">
                  <textarea class="form-control " id="task" name="todolist" required></textarea>
                </div>
              </div>
              <div class="form-group ">
                <label for="ctype" class="control-label col-lg-2">Status <span class="required">*</label>
                <div id="ctype" class="col-lg-10">
                  <select class="form-control m-bot15" name="status">
                      <option value="proses">Proses</option>
                      <option value="bug">Bug</option>
                      <option value="selesai">Selesai</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  <button class="btn btn-primary btn-block" type="submit">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
    </div>

  </div>
 </section>
