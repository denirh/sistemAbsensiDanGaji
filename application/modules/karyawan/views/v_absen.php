<section>
  <div class="row">
    <div class="col-lg-6">
      <!-- Form Absensi -->
        <header class="panel-heading">
          Form Absensi
        </header>
        <div class="panel-body">
          <div class="form">
            <form class="form-validate form-horizontal" id="feedback_form" method="POST" action="<?= base_url() ?>index.php/karyawan/create_data/">
              <!-- <div class="form-group " >
                <label for="cname" class="control-label col-lg-2">ID<span class="required">*</span></label>
                <div class="col-lg-10">
                  <input class="form-control" id="cname" name="id_karyawan" type="number" required />
                </div>
              </div> -->
              <div class="form-group ">
                <label for="cemail" class="control-label col-lg-2">Tanggal <span class="required">*</span></label>
                <div class="col-lg-10">
                  <input class="form-control " id="cemail" type="datetime-local" name="tanggal_hadir" required />
                </div>
              </div>
              <div class="form-group ">
                <label for="ctype" class="control-label col-lg-2">Ket <span class="required">*</label>
                <div id="ctype" class="col-lg-10">
                  <select class="form-control m-bot15" name="type">
                      <option value="in">In</option>
                      <option value="out">Out</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  <button class="btn btn-primary btn-block" type="submit">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
    </div>
    <div class="col-lg-6">
      <header class="panel-heading">
        History Absensi
      </header>
      <table class="table table-striped table-advance table-hover table-bordered" style="margin-top: 13px;">
          <tbody>
            <tr>
              <th><i class="icon_profile"></i> ID</th>
              <th><i class="fa fa-archive"></i> Tanggal</th>
              <th><i class="icon_mail_alt"></i> Keterangan</th>
            </tr>
            <?php
                foreach ($absen_history as $row) {
            ?>
            <tr>
              <td class="text-center"><?= $row->presensi_karyawan_id ?></td>
              <td><?= $row->presensi_date ?></td>
              <td><?= $row->presensi_type ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
    </div>
  </div>
</section>
