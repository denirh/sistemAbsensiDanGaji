<section>
  <div class="row">
    <div class="col-lg-12">
      <!-- Form Absensi -->
        <header class="panel-heading">
          Edit To Do List
        </header>
        <div class="panel-body">
          <div class="form">
            <?php 
                foreach($todolist as $row) { 
            ?>
            <form class="form-validate form-horizontal" id="feedback_form" method="POST" action="<?= base_url() ?>index.php/karyawan/update_todolist/">
              <div class="form-group ">
                <label for="cname" class="control-label col-lg-2">ID Karyawan</label>
                <div class="col-lg-10">
                  <input class="form-control" id="cname" name="id_task" type="hidden" value="<?= $row->task_id ?>" />
                </div>
              </div>
              <div class="form-group ">
                <label for="tgl" class="control-label col-lg-2">Tanggal</label>
                <div class="col-lg-10">
                  <input class="form-control" id="tgl" name="tanggal_task" type="date" value="<?= $row->task_tanggal ?>" />
                </div>
              </div>
              <div class="form-group ">
                <label for="task" class="control-label col-lg-2">To Do List <span class="required">*</label>
                <div class="col-lg-10">
                  <textarea class="form-control " id="task" name="todolist" required><?= $row->task_desc ?></textarea>
                </div>
              </div>
              <div class="form-group ">
                <label for="ctype" class="control-label col-lg-2">Status <span class="required">*</label>
                <div id="ctype" class="col-lg-10">
                  <select class="form-control m-bot15" name="status">
                      <option value="<?= $row->task_percent ?>"><?= $row->task_percent ?></option>
                      <?php for($i=0; $i<count($array_option); $i++){
                                if($array_option[$i] != $row->task_percent ){

                      ?>
                        <option value="<?= $array_option[$i] ?>"><?= $array_option[$i] ?></option>
                     <?php }} ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  <button class="btn btn-primary btn-block" type="submit">Submit</button>
                </div>
              </div>
            </form>
            <?php } ?>
          </div>
        </div>
    </div>

  </div>
 </section>
