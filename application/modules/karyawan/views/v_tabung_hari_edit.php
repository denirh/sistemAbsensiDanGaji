<section>
    <div class="col-lg-12">
      <!-- Form Todolist -->
        <header class="panel-heading">
          Edit Tabung Hari
        </header>
        <div class="panel-body">
          <div class="form">
            <?php 
              foreach ($nabung as $row) {
                
            ?>
            <form class="form-validate form-horizontal" id="feedback_form" method="POST" action="<?= base_url() ?>index.php/karyawan/update_tabung_hari/">
              <div class="form-group " hidden>
                <label for="task" class="control-label col-lg-2">ID <span class="required">*</label>
                <div class="col-lg-10">
                  <input class="form-control" type="text" id="task" name="id_pres" value="<?= $row->presensi_id ?>">
                </div>
              </div>
              <div class="form-group ">
                <label for="task" class="control-label col-lg-2">Tanggal <span class="required">*</label>
                <div class="col-lg-10">
                  <input class="form-control" type="date" id="task" name="tanggal_tabung" value="<?= $row->presensi_tabung_hari ?>" readonly>
                </div>
              </div>
              <div class="form-group ">
                <label for="ctype" class="control-label col-lg-2">Status <span class="required">*</label>
                <div id="ctype" class="col-lg-10">
                  <select class="form-control m-bot15" name="tabungan" readonly>
                      <option value="nabung">Tabung Hari</option>
                  </select>
                </div>
              </div>
              <div class="form-group ">
                <label for="task" class="control-label col-lg-2">Alasan <span class="required">*</label>
                <div class="col-lg-10">
                  <textarea class="form-control " id="task" name="desc_tabung" required><?= $row->presensi_desc_tabung_hari ?></textarea>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  <button class="btn btn-primary btn-block" type="submit">Submit</button>
                </div>
              </div>
            </form>
            <?php } ?>
          </div>
        </div>
    </div>

  </div>
 </section>
