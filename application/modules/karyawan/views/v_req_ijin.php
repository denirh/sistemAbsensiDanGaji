<section>
  <div class="row">
    <div class="col-lg-12" style="margin-top: 20px;">
      <header class="panel-heading">
        History Ijin
      </header>
      <table class="table table-striped table-advance table-hover table-bordered" style="margin-top: 13px;">
          <tbody>
            <tr>
              <th><i class="icon_profile"></i> ID</th>
              <th><i class="fa fa-archive"></i> Tanggal</th>
              <th><i class="fa fa-archive"></i> Alasan</th>
              <th><i class="icon_mail_alt"></i> Status</th>
            </tr>
            <?php
                foreach ($ijin_history as $row) {
                  $status = $row->presensi_ijin_status;

                  if($status == 'waiting'){
                    $status_label = 'info';
                    $status_desc  = 'Waiting';
                  }elseif($status == 'approve'){
                    $status_label = 'success';
                    $status_desc  = 'Approve';
                  }elseif($status == 'reject'){
                    $status_label = 'danger';
                    $status_desc  = 'Reject';
                  }else{
                    $status_label = '';
                    $status_desc  = '';
                  }

            ?>
            <tr>
              <td class="text-center"><?= $row->presensi_karyawan_id ?></td>
              <td><?= $row->presensi_acc_ijin ?></td>
              <td><?= $row->presensi_desc_ijin ?></td>
              <td>
                  <span class="label label-<?php echo $status_label ?>" style="font-size: 12px; border-radius: 0;"><?php echo $status_desc ?></span>
              </td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
    </div>
    <div class="col-lg-12">
      <!-- Form Ijin -->
        <header class="panel-heading">
          Form Ijin
        </header>
        <div class="panel-body">
          <div class="form">
            <form class="form-validate form-horizontal" id="feedback_form" method="POST" action="<?= base_url() ?>index.php/karyawan/create_req_izin/">
              <div class="form-group ">
                <label for="cemail" class="control-label col-lg-2">Tanggal Ijin <span class="required">*</span></label>
                <div class="col-lg-10">
                  <input class="form-control " id="cemail" type="datetime-local" name="tanggal_ijin" required />
                </div>
              </div>
              <div class="form-group ">
                <label for="ijin" class="control-label col-lg-2">Alasan Ijin</label>
                <div class="col-lg-10">
                  <textarea class="form-control " id="ijin" name="alasan" required></textarea>
                </div>
              </div>


              <div class="form-group ">
                <label for="ctype" class="control-label col-lg-2">Status <span class="required">*</label>
                <div id="ctype" class="col-lg-10">
                  <select class="form-control m-bot15" name="status">
                      <option value="waiting">Waiting</option>
                  </select>
                </div>
              </div>


              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  <button class="btn btn-primary btn-block" type="submit">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
    </div>
    
  </div>
</section>
