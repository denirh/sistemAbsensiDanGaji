<section>
  <div class="row">
    <div class="col-lg-12" style="margin-top: 20px;">
      <header class="panel-heading">
        History Lembur
      </header>
      <table class="table table-striped table-advance table-hover table-bordered" style="margin-top: 13px;">
          <tbody>
            <tr>
              <th><i class="icon_profile"></i> ID</th>
              <th><i class="fa fa-archive"></i> Tanggal</th>
              <th><i class="fa fa-archive"></i> Lama (Jam)</th>
              <th><i class="icon_mail_alt"></i> Task Yang Dikerjakan</th>
            </tr>
             <?php
                foreach ($lembur_history as $row) {
            ?>
            <tr>
              <td><?= $row->presensi_karyawan_id ?></td>
              <td><?= $row->presensi_date ?></td>
              <td><?= $row->presensi_lama_lembur ?> Jam</td>
              <td><?= $row->presensi_desc_lembur ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
    </div>
    <div class="col-lg-12">
      <!-- Form Ijin -->
        <header class="panel-heading">
          Form Lembur
        </header>
        <div class="panel-body">
          <div class="form">
            <form class="form-validate form-horizontal" id="feedback_form" method="POST" action="<?= base_url() ?>index.php/karyawan/create_lembur/">
              <div class="form-group ">
                <label for="cemail" class="control-label col-lg-2">Tanggal <span class="required">*</span></label>
                <div class="col-lg-10">
                  <input class="form-control " id="cemail" type="datetime-local" name="tanggal_lembur" required />
                </div>
              </div>
              <div class="form-group ">
                <label for="ctype" class="control-label col-lg-2">Status <span class="required">*</label>
                <div id="ctype" class="col-lg-10">
                  <select class="form-control m-bot15" name="lemburan">
                      <option value="lembur">Lembur</option>
                  </select>
                </div>
              </div>
              <div class="form-group ">
                <label for="lembur" class="control-label col-lg-2">Lama Waktu (Jam)</label>
                <div class="col-lg-10">
                  <input type="number" class="form-control" id="lembur" name="lama_lembur" required=""> 
                </div>
              </div>
              <div class="form-group ">
                <label for="task" class="control-label col-lg-2">Task Lembur <span class="required">*</label>
                <div class="col-lg-10">
                  <textarea class="form-control " id="task" name="task_lembur" required></textarea>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  <button class="btn btn-primary btn-block" type="submit">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
    </div>
    
  </div>
</section>
