<section>
  <div class="row">
	  <div class="col-lg-12" style="margin-top: 20px;">
      <section class="panel">
        <header class="panel-heading">
          Informasi Tabung Hari
        </header>

        <table class="table table-striped table-advance table-hover">
          <tbody>
            <tr>
              <th><i class="icon_profile"></i> ID</th>
              <th>Tanggal</th>
              <th>Alasan</th>
              <th>Action</th>
            </tr>
            <?php 
            	foreach ($tabung_history as $row) {
            		$format = date('d F Y', strtotime($row->presensi_tabung_hari ));
            		
            ?>
            <tr>
              <td><?= $row->presensi_karyawan_id ?></td>
              <td><?= $format ?></td>
              <td><?= $row->presensi_desc_tabung_hari ?></td>
              <td>
              	  <a class="btn btn-success" href="<?= base_url() ?>index.php/karyawan/edit_tabung_hari/<?= $row->presensi_id ?>"><i class="fa fa-edit"></i></a>
              </td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </section>
    </div>

    <div class="col-lg-12">
      <!-- Form Todolist -->
        <header class="panel-heading">
          Buat Tabung Hari
        </header>
        <div class="panel-body">
          <div class="form">
            <form class="form-validate form-horizontal" id="feedback_form" method="POST" action="<?= base_url() ?>index.php/karyawan/create_tabung_hari/">
              <div class="form-group ">
                <label for="task" class="control-label col-lg-2">Tanggal <span class="required">*</label>
                <div class="col-lg-10">
                  <input class="form-control" type="date" id="task" name="tanggal_tabung" required>
                </div>
              </div>
              <div class="form-group ">
                <label for="ctype" class="control-label col-lg-2">Status <span class="required">*</label>
                <div id="ctype" class="col-lg-10">
                  <select class="form-control m-bot15" name="tabungan">
                      <option value="nabung">Tabung Hari</option>
                  </select>
                </div>
              </div>
              <div class="form-group ">
                <label for="task" class="control-label col-lg-2">Alasan <span class="required">*</label>
                <div class="col-lg-10">
                  <textarea class="form-control " id="task" name="desc_tabung" required></textarea>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  <button class="btn btn-primary btn-block" type="submit">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
    </div>

  </div>
 </section>
