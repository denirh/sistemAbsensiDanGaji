<?php 
	class Mdl_karyawan extends CI_Model {

function __construct() {
	parent::__construct();
}

function tabel_presensi($where=''){
	$query = $this->db->get('presensi' .$where);	
	return $query->result();
}

function input_data($data,$table){
	$this->db->insert($table,$data);
}

function input_req_ijin($data,$table){
	$this->db->insert($table,$data);
}

function tabel_task($where=''){
	$query = $this->db->get('task' .$where);
	return $query->result();
}

function input_todolist($data,$table){
	$this->db->insert($table,$data);
}

function edit_todolist($where,$table){		
	return $this->db->get_where($table,$where);
}

function update_todolist($where,$data,$table){
	$this->db->where($where);
	$this->db->update($table,$data);
}

function hapus_todolist($where,$table){
	$this->db->where($where);
	$this->db->delete($table);
}

function lihat_ijin($where=''){
	$query = $this->db->get("presensi" .$where);
	return $query->result();
}

function input_lembur($data,$table){
	$this->db->insert($table,$data);
}

function history_lembur($where=''){
	$query = $this->db->get('presensi' .$where);
	return $query->result();
}

function input_tabung_hari($data,$table){
	$this->db->insert($table,$data);
}

function show_tabung_hari($where=''){
	$query = $this->db->get('presensi' .$where);	
	return $query->result();
}

function edit_nabung($where,$table){		
	return $this->db->get_where($table,$where);
}

function update_nabung($where,$data,$table){
	$this->db->where($where);
	$this->db->update($table,$data);
}


}
