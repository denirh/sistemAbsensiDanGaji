<?php 
	class Mdl_manager extends CI_Model {

function __construct() {
	parent::__construct();
}

function tabel_karyawan(){
	$query = $this->db->get('karyawan');	
	return $query->result();
}

function input_data($data,$table){
	$this->db->insert($table,$data);
}

function edit_data($where,$table){		
	return $this->db->get_where($table,$where);
}

function update_data($where,$data,$table){
	$this->db->where($where);
	$this->db->update($table,$data);
}

function hapus_data($where,$table){
	$this->db->where($where);
	$this->db->delete($table);
}

function tabel_presensi(){
	$query = $this->db->get('presensi');	
	return $query->result();
}

function edit_ijin($where,$table){		
	return $this->db->get_where($table,$where);
}

function update_ijin($where,$data,$table){
	$this->db->where($where);
	$this->db->update($table,$data);
}

function tabel_task(){
	$query = $this->db->get('task');	
	return $query->result();
}

function permintaan_ijin(){
	$query = $this->db->query("SELECT * FROM presensi WHERE presensi_ijin_status");
	return $query->result();
}


}
