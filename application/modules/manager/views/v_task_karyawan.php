<section>
  <div class="row">
	  <div class="col-lg-12" style="margin-top: 20px;">
      <section class="panel">
        <header class="panel-heading">
          Informasi Task Karyawan
        </header>

        <table class="table table-striped table-advance table-hover">
          <tbody>
            <tr>
              <th><i class="icon_profile"></i> ID Karyawan</th>
              <th><i class="icon_profile"></i> Tanggal</th>
              <th>Task Karyawan</th>
              <th>Status</th>
            </tr>
            <?php
            	foreach ($task_karyawan as $row) {
            		$status = $row->task_percent;
                $format = date('d F Y', strtotime($row->task_tanggal ));

	            	  if($status == 'proses'){
	                    $status_label = 'info';
	                    $status_desc  = 'Proses';
	                  }elseif($status == 'selesai'){
	                    $status_label = 'success';
	                    $status_desc  = 'Selesai';
	                  }elseif($status == 'bug'){
	                    $status_label = 'danger';
	                    $status_desc  = 'Ada Bug';
	                  }else{
	                    $status_label = '';
	                    $status_desc  = '';
	                  }
            ?>
            <tr>
              <td><?= $row->task_presensi_id ?></td>
              <td><?= $format ?></td>
              <td><?= $row->task_desc ?></td>
              <td>
              	  <span class="label label-<?php echo $status_label ?>" style="font-size: 12px; border-radius: 0;"><?php echo $status_desc ?></span>
              </td>
            </tr>
			<?php } ?>
          </tbody>
        </table>
      </section>
    </div>
  </div>
</section>