<section>
  <a class="btn btn-add btn-primary" data-toggle="modal" data-target="#addModal" style="margin-bottom: 20px;">
    <i class="fa fa-plus"></i> Tambah Data
  </a>

  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          Informasi Karyawan
        </header>

        <table class="table table-striped table-advance table-hover">
          <tbody>
            <tr>
              <th><i class="icon_profile"></i> ID</th>
              <th><i class="icon_profile"></i> Nama</th>
              <th><i class="icon_pin_alt"></i> Alamat</th>
              <th><i class="icon_mail_alt"></i> Email</th>            
              <th><i class="icon_mobile"></i> No. HP</th>                        
              <th><i class="fa fa-dollar"></i> Jabatan</th>
              <th><i class="icon_cogs"></i> Action</th>
            </tr>
            <?php
                foreach ($list_karyawan as $row) {

            ?>
            <tr>
              <td><?= $row->karyawan_id; ?></td>
              <td><?= $row->karyawan_name; ?></td>
              <td><?= $row->karyawan_adress; ?></td>
              <td><?= $row->karyawan_mail; ?></td>
              <td><?= $row->karyawan_phone; ?></td>
              <td><?= $row->karyawan_level; ?></td>
              <td>
                <div class="btn-group">
                  <a class="btn btn-success" href="<?= base_url() ?>index.php/manager/edit/<?= $row->karyawan_id ?>"><i class="fa fa-edit"></i></a>
                  <a class="btn btn-danger" href="<?= base_url() ?>index.php/manager/hapus/<?= $row->karyawan_id ?>"><i class="icon_close_alt2"></i></a>
                </div>
              </td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </section>
    </div>
  </div>

      <!-- add data modal -->
      <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h4 class="modal-title" id="addModalLabel" style="color: #007aff; font-weight: 700;"> Add Data</h4>
                  </div>
                  <div class="modal-body">                                            
                      <form action="<?= base_url() ?>index.php/manager/create_data/" method="post">
                        <div class="form-group create-data">
                          <label>Nama</label>
                          <input class="form-control data-input" name="nama" placeholder="Masukkan Nama" required="">
                        </div>
                        <div class="form-group create-data">
                          <label>Alamat</label>
                          <input class="form-control data-input" name="alamat" placeholder="Masukkan Alamat" required="">
                        </div>
                        <div class="form-group create-data">
                          <label>Email</label>
                          <input class="form-control data-input" name="email" placeholder="Masukkan Email" required="">
                        </div>                                              
                        <div class="form-group create-data">
                          <label>No. HP</label>
                          <input class="form-control data-input" name="nohp" placeholder="Masukkan Nomor HP" required="">
                        </div>
                        <div class="form-group create-data">
                          <label>Password</label>
                          <input class="form-control data-input" name="password" placeholder="Masukkan Password" required=""> 
                        </div>
                        <div class="form-group create-data">
                          <label>Hak Akses</label>
                          <select class="form-control" name="level">
                              <option value="karyawan">Karyawan</option>
                              <option value="manager">Manager</option>
                              <option value="client">Client</option>
                          </select>
                        </div>
                  </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                          <input type="submit" name="simpan" value="Simpan Data" class="btn btn-primary">
                        </div>
                      </form>
              </div>
          </div>
      </div>

</section>
