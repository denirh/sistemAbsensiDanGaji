<div class="col-lg-12" style="margin-top: 20px;">
  <header class="panel-heading">
    Keputusan Ijin
  </header>
  <div class="panel-body">
    <div class="form">
      <?php 
         foreach ($ijin as $row) {
      ?>
      <form class="form-validate form-horizontal" id="feedback_form" method="POST" action="<?= base_url() ?>index.php/manager/update_ijin_status/">
        <div class="form-group ">
          <label for="cname" class="control-label col-lg-2">ID Karyawan </label>
          <div class="col-lg-10">
            <input class="form-control" id="cname" name="id" value="<?= $row->presensi_id ?>" type="hidden" />
            <input class="form-control" id="cname" value="<?= $row->presensi_karyawan_id ?>" readonly/>
          </div>
        </div>
        <div class="form-group ">
          <label for="cemail" class="control-label col-lg-2">Tanggal Ijin </label>
          <div class="col-lg-10">
            <input class="form-control " id="cemail" value="<?= $row->presensi_acc_ijin ?>" readonly/>
          </div>
        </div>
        <div class="form-group ">
          <label for="ijin" class="control-label col-lg-2">Alasan Ijin</label>
          <div class="col-lg-10">
            <textarea class="form-control " id="ijin" readonly><?= $row->presensi_desc_ijin ?></textarea>
          </div>
        </div>

        <div class="form-group ">
          <label for="ctype" class="control-label col-lg-2">Status </label>
          <div id="ctype" class="col-lg-10">
            <select class="form-control m-bot15" name="status">
                <option value="<?= $row->presensi_ijin_status ?>"><?= $row->presensi_ijin_status ?></option>
                  <?php for($i=0; $i<count($array_option); $i++){
                            if($array_option[$i] != $row->presensi_ijin_status ){

                  ?>
                <option value="<?= $array_option[$i] ?>"><?= $array_option[$i] ?></option>
                 <?php }} ?>
            </select>
          </div>
        </div>


        <div class="form-group">
          <div class="col-lg-offset-2 col-lg-10">
            <button class="btn btn-primary btn-block" type="submit">Submit</button>
          </div>
        </div>
      </form>
      <?php } ?>
    </div>
  </div>
</div>