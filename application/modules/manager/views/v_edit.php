 <!-- edit data  -->
<div class="edit-data-wrapper">
    <div class="edit-content">
        <div class="edit">
            <div class="edit-header">
                <h4 class="edit-title" style="color: #007aff; font-weight: 700;"> Update Data</h4>
            </div>
            <div class="edit-body"> 
            <?php 
                foreach($karyawan as $row) { 
            ?>                                           
                <form action="<?= base_url() ?>index.php/manager/update/" method="post">
                  <div class="form-group create-data">
                    <label>Nama</label>
                    <input type="hidden" name="id" value="<?php echo $row->karyawan_id ?>">
                    <input class="form-control data-input" name="nama" value="<?= $row->karyawan_name ?>">
                  </div>
                  <div class="form-group create-data">
                    <label>Alamat</label>
                    <input class="form-control data-input" name="alamat" value="<?= $row->karyawan_adress ?>">
                  </div>
                  <div class="form-group create-data">
                    <label>Email</label>
                    <input class="form-control data-input" name="email" value="<?= $row->karyawan_mail ?>">
                  </div>                                              
                  <div class="form-group create-data">
                    <label>No. HP</label>
                    <input class="form-control data-input" name="nohp" value="<?= $row->karyawan_phone ?>">
                  </div>
                  <div class="form-group create-data">
                    <label>Password</label>
                    <input class="form-control data-input" name="password" value="<?= $row->karyawan_password ?>"> 
                  </div>
                  <div class="form-group create-data">
                    <label>Hak Akses</label>
                    <input class="form-control data-input" name="level" value="<?= $row->karyawan_level ?>" readonly> 
                  </div>
            </div>
                  <div class="edit-footer">
                    <a href="<?= base_url() ?>index.php/manager/data_karyawan/" type="button" class="btn btn-default" data-dismiss="modal">Batal</a>
                    <input type="submit" name="simpan" value="Simpan Data" class="btn btn-primary">
                  </div>
                </form>
            <?php } ?>
        </div>
    </div>
</div>