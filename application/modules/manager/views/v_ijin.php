<div class="col-lg-12" style="margin-top: 20px;">
  <header class="panel-heading">
    Permintaan Ijin Karyawan
  </header>
  <table class="table table-striped table-advance table-hover table-bordered" style="margin-top: 13px;">
      <tbody>
        <tr>
          <th><i class="icon_profile"></i> ID</th>
          <th><i class="fa fa-archive"></i> Tanggal</th>
          <th><i class="fa fa-archive"></i> Alasan</th>
          <th><i class="fa fa-archive"></i> Status</th>
          <th><i class="icon_mail_alt"></i> Actions</th>
        </tr>
        <?php 
            foreach($req_ijin as $row){
              $status = $row->presensi_ijin_status;

                  if($status == 'waiting'){
                    $status_label = 'info';
                    $status_desc  = 'Waiting';
                  }elseif($status == 'approve'){
                    $status_label = 'success';
                    $status_desc  = 'Approve';
                  }elseif($status == 'reject'){
                    $status_label = 'danger';
                    $status_desc  = 'Reject';
                  }else{
                    $status_label = '';
                    $status_desc  = '';
                  }
        ?>
        <tr>
          <td class="text-center"><?= $row->presensi_karyawan_id ?></td>
          <td><?= $row->presensi_acc_ijin ?></td>
          <td><?= $row->presensi_desc_ijin ?></td>
          <td>
              <span class="label label-<?php echo $status_label ?>" style="font-size: 12px; border-radius: 0;"><?php echo $status_desc ?></span>
          </td>
          <td>
            <a class="btn btn-success" href="<?= base_url() ?>index.php/manager/ijin_edit/<?= $row->presensi_id ?>"><i class="fa fa-edit"></i> Ubah</a>
          </td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
</div>