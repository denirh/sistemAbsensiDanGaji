<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
	class Manager extends MX_Controller
{

function __construct() {
	parent::__construct();
	$this->load->model('mdl_manager');
	$this->load->model('mdl_aman');
	$this->load->helper('url');
}

function task_karyawan(){
	$show_data = $this->mdl_manager->tabel_task(); // menampilkan data tabel
	$data['task_karyawan'] = $show_data;
	$data['data'] = $this->load->view('v_task_karyawan',$data, TRUE);
	$this->load->view('templates/v_header_templates');
	$this->load->view('templates/v_body_templates', $data);
	$this->load->view('templates/v_footer_templates');
}

function update_ijin_status(){	
	$data = array(
		'presensi_ijin_status'  => $this->input->post('status')
	); 
	$where = array(
		'presensi_id' => $this->input->post('id')
	);
 
	$this->mdl_manager->update_ijin($where,$data,'presensi');
	redirect('manager/permintaan_ijin/');

}

function ijin_edit($id){
	$data['array_option'] = array('waiting','approve','reject');
	$where = array('presensi_id' => $id);
	$data['ijin'] = $this->mdl_manager->edit_ijin($where,'presensi')->result();
	$data['data'] = $this->load->view('v_ijin_edit',$data, TRUE);
	$this->load->view('templates/v_header_templates');
	$this->load->view('templates/v_body_templates', $data);
	$this->load->view('templates/v_footer_templates');
}


function permintaan_ijin(){
	$show_data = $this->mdl_manager->permintaan_ijin(); // menampilkan data tabel
	$data['req_ijin'] = $show_data;
	$data['data'] = $this->load->view('v_ijin',$data,TRUE);
	$this->load->view('templates/v_header_templates');
	$this->load->view('templates/v_body_templates', $data);
	$this->load->view('templates/v_footer_templates');
}

function hapus($id){
	$where = array('karyawan_id' => $id);
	$this->mdl_manager->hapus_data($where,'karyawan');
	redirect('manager/data_karyawan');
}

function update(){
	
	$data = array(
		'karyawan_name'     => $this->input->post('nama'),
		'karyawan_adress'   => $this->input->post('alamat'),
		'karyawan_mail'     => $this->input->post('email'),
		'karyawan_phone'    => $this->input->post('nohp'),
		'karyawan_password' => $this->input->post('password'),
		'karyawan_level'    => $this->input->post('level')
	);
 
	$where = array(
		'karyawan_id' => $this->input->post('id')
	);
 
	$this->mdl_manager->update_data($where,$data,'karyawan');
	redirect('manager/data_karyawan');
}

function edit($id){
	$where = array('karyawan_id' => $id);
	$data['karyawan'] = $this->mdl_manager->edit_data($where,'karyawan')->result();
	$data['data'] = $this->load->view('v_edit',$data, TRUE);
	$this->load->view('templates/v_header_templates');
	$this->load->view('templates/v_body_templates', $data);
	$this->load->view('templates/v_footer_templates');
}

function data_karyawan(){
	$this->mdl_aman->halaman_aman();
	$usersession = $this->session->userdata('id');
	$show_data = $this->mdl_manager->tabel_presensi(" WHERE presensi_karyawan_id='$usersession' "); // menampilkan data tabel

	foreach($show_data as $row){
		$data['views'] = $row->presensi_karyawan_id;
	}

	$show_data = $this->mdl_manager->tabel_karyawan(); // menampilkan data tabel
	$data['list_karyawan'] = $show_data;
	$data['data'] = $this->load->view('v_data_karyawan', $data, TRUE);
	$this->load->view('templates/v_header_templates');
	$this->load->view('templates/v_body_templates', $data);
	$this->load->view('templates/v_footer_templates');
}

function create_data(){
	$this->load->helper('form');

	$data = array(
		'karyawan_name'    => $this->input->post('nama'),
		'karyawan_adress'  => $this->input->post('alamat'),
		'karyawan_mail'    => $this->input->post('email'),
		'karyawan_phone'   => $this->input->post('nohp'),
		'karyawan_password' => $this->input->post('password'),
		'karyawan_level'    => $this->input->post('level')
		);

	$this->mdl_manager->input_data($data,'karyawan');

	redirect('manager/data_karyawan');
}

function index(){
	$this->mdl_aman->halaman_aman();
	$data['data'] = $this->load->view('v_index','',TRUE);
	$this->load->view('templates/v_header_templates');
	$this->load->view('templates/v_body_templates', $data);
	$this->load->view('templates/v_footer_templates');
}

}
