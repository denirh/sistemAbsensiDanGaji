<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
	class Login extends MX_Controller
{

function __construct() {
	parent::__construct();
}

public function index(){

	$this->load->view('login/v_login.php');
}
		
public function cek_login() {	
	
	$user  = $_POST['id'];
	$pwd   = $_POST['password'];
	$level = $_POST['level'];

	$this->load->model('mdl_login');

	$result=$this->mdl_login->getlogin($user,$pwd,$level)->row();
	//echo $result->karyawan_id;

	if($result)
	{
		 // foreach ($result as $row)
		 // {

if(($level=="karyawan")|| ($level=="manager") || ($level=="client")){
		 	$sess = array();
				$sess['id']           = $result->karyawan_id;
				$sess['level']        = $level;
				$sess['is_logged_in'] ='1';
				
		 	$this->session->set_userdata($sess);
		}


		 	if($this->session->userdata('level') == 'karyawan')
			 	{
			 	 	redirect('karyawan/');	
			 	}

		 	elseif($this->session->userdata('level') == 'manager')
			 	{
			 		redirect('manager/');
			 	}

			elseif($this->session->userdata('level') == 'client')
			 	{
			 		redirect('client/');
			 	}

			 else
				{
				 	redirect('login/');
				}
		 				 
		 // }
	}
	else
	{
		$this->session->set_flashdata('pesan','Oops !!  Username or password error. Please try again');
		redirect('login');
	}
	if(($username='NULL'))
	{  
		$this->session->sess_destroy();
		return $user;
		redirect('login');
	}
	
}

public function logout() {
		$this->session->sess_destroy();
		redirect('login/');

	}

}