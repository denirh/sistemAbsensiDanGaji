<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Halaman Login</title>

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700|Lato:400,100,300,700,900' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="<?= base_url() ?>login_template/css/animate.css">
	<!-- Custom Stylesheet -->
	<link rel="stylesheet" href="<?= base_url() ?>login_template/css/style.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
</head>

<body>
	<div class="container">
		<div class="login-box animated fadeInUp">
			<div class="box-header">
				<h2>Log In</h2>
			</div>
			  <form action="<?php echo base_url()."index.php/login/cek_login/" ?>" method='post'>
				<label for="user">ID Karyawan</label>
				<br/>
				<input type="text" name="id" id="user" required="">
				<br/>
				<label for="passw">Password</label>
				<br/>
				<input type="password" name="password" id="pass" required="">
				<br/>
				<div class="form-group">                             
	                <label>
	                    <input type="radio" name="level" value="manager" class="minimal" checked/> &nbsp; Manager &nbsp;&nbsp;
	                </label>
	                <label>
	                    <input type="radio" name="level" value="karyawan" class="minimal"/>&nbsp; Karyawan &nbsp;&nbsp;
	                </label>
	                <label>
	                    <input type="radio" name="level" value="client" class="minimal" /> &nbsp;Client
	                </label>
	            </div>
	            <br/>
				<button type="submit">Sign In</button>
				<br/>
				<a href="#"><p class="small">Forgot your password?</p></a>
			  </form>
		</div>
	</div>
</body>

<script>
	$(document).ready(function () {
    	$('#logo').addClass('animated fadeInDown');
    	$("input:text:visible:first").focus();
	});
	$('#username').focus(function() {
		$('label[for="username"]').addClass('selected');
	});
	$('#username').blur(function() {
		$('label[for="username"]').removeClass('selected');
	});
	$('#password').focus(function() {
		$('label[for="password"]').addClass('selected');
	});
	$('#password').blur(function() {
		$('label[for="password"]').removeClass('selected');
	});
</script>

</html>